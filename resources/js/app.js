
require('./bootstrap');

window.Vue = require('vue');

import vuetify from './vuetify';
import router from './router';

//! Vue.component('example-component', require('./components/ExampleComponent.vue').default);

import app from './components/AppComponent';


new Vue({
    el: '#app',
    router,
    vuetify,
    components: {
        'App' : app,
    }
});
