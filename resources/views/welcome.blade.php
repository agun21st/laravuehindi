<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Laravel With Vue</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <!-- Fonts impoted on app.scss -->
        
    </head>
    <body>
        
        <div id="app">
            <App />
        </div>

        <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    </body>
</html>
